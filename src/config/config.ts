import * as z from "zod";
import { nanoid } from "nanoid";
import { unixPathSchema } from "./schemas/util";

export const configSchema = z.object({
    http: z.object({
        host: z.string().default("0.0.0.0"),
        port: z.number().default(80),
    }),
    https: z
        .object({
            cert: unixPathSchema,
            key: unixPathSchema,
            ca: unixPathSchema,
            host: z.string().default("0.0.0.0"),
            port: z.number().default(443),
        })
        .optional(),
    auth: z.boolean(),
    apiKey: z
        .string()
        .min(5)
        .default(() => nanoid(20)),
    providers: z.array(z.string().url()),
});

export type Config = z.infer<typeof configSchema>;
