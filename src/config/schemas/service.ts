import * as z from "zod";
import { routeSchema } from "./route";

export const serviceSchema = z.object({
    domain: z.union([z.string(), z.array(z.string())]).optional(),
    path: z.string().default("/"),
    rewrite: z
        .object({
            from: z.string().default("/"),
            to: z.string(),
        })
        .optional(),
    auth: z.boolean().optional(),
    routes: z.union([z.array(routeSchema), routeSchema]),
});

export type Service = z.infer<typeof serviceSchema>;
