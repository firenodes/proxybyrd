import * as z from "zod";
import { unixPathSchema } from "./util";

export const routeSchema = z.union([
    z.object({ type: z.literal("proxy"), url: z.string() }),
    z.object({
        type: z.union([z.literal("render"), z.literal("serve")]),
        path: unixPathSchema,
    }),
    z.object({
        type: z.literal("send"),
        data: z.unknown(),
        status: z.number().default(200),
    }),
]);

export type Route = z.infer<typeof routeSchema>;
