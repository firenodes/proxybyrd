import * as z from "zod";
import { serviceSchema } from "./service";

export const providerSchema = z.object({
    sites: z.record(serviceSchema).default({}),
});

export type Provider = z.infer<typeof providerSchema>;
