import { WriteConfig } from "@theoparis/config";
import { Config } from "./config";

export interface Options {
    configPath: string;
    config: WriteConfig<Config>;
}
