import { Config, configSchema } from "./config";
import { readFile } from "@theoparis/config";
import path from "upath";
import { logger } from "../logger";
import fs from "fs";

export const config = () => {
    const configPath = path.join(
        process.cwd(),
        process.env.PROXY_BYRD_CONFIG || "config.yml"
    );
    if (!fs.existsSync(configPath)) {
        logger.error(`Config file doesn't exist at: ${configPath}`);
        process.exit(1);
    }
    return {
        config: readFile<Config>(configPath, {
            type: "yaml",
            schema: configSchema,
        }).validate(true, (err) => {
            logger.error(err);
            process.exit(1);
        }),
        configPath,
    };
};
