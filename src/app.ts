import express, { Application } from "express";
import fs from "fs";
import path from "upath";
import https from "https";
import http from "http";
import fetch from "node-fetch";

import cors from "cors";
import yaml from "js-yaml";
import { handleService } from "./core/handlers";
import { logger } from "./logger";
import { providerSchema } from "./config";
import { Options } from "./config/options";

export const hasValidScheme = (s: string) => /^https?:\/\//.test(s);

export interface AppResult {
    app: Application;
}

export const createApp = (opts: Options): AppResult => {
    const app = express();
    app.use(cors());
    const cfg = opts.config;
    cfg.toObject().providers.forEach((providerUrl) => {
        const cleanURL = providerUrl.replace(/file:\/\/?/, "");
        if (cleanURL.startsWith("./")) {
            const file = path.join(path.dirname(opts.configPath), cleanURL);
            const raw = fs.readFileSync(file, { encoding: "utf-8" });
            const provider = providerSchema.parse(yaml.load(raw));
            Object.entries(provider.sites).forEach(
                handleService({ app, config: cfg, configPath: opts.configPath })
            );
        } else
            fetch(cleanURL)
                .then((res) => res.text())
                .then((raw) => {
                    const provider = providerSchema.parse(yaml.load(raw));
                    Object.entries(provider.sites).forEach(
                        handleService({
                            app,
                            config: cfg,
                            configPath: opts.configPath,
                        })
                    );
                });
    });
    return { app };
};

export interface ServerResult extends AppResult {
    httpServer: http.Server;
    httpsServer: https.Server;
}

export const createServer = (opts: Options): ServerResult => {
    const { app, ...res } = createApp(opts);
    const cfg = opts.config;

    let httpServer: http.Server;
    let httpsServer: https.Server;

    function showAddress(type: string) {
        const bound = {
            address:
                type === "http"
                    ? cfg.toObject().http.host
                    : cfg.toObject().https.host,
            port:
                type === "http"
                    ? cfg.toObject().http.port
                    : cfg.toObject().https.port,
        };
        return () =>
            logger.info(
                `Listening on ${type}://${bound.address}:${bound.port}`
            );
    }

    if (cfg.toObject().http || !cfg.toObject().https)
        httpServer = app.listen(
            cfg.toObject().http.port || 8080,
            cfg.toObject().http.host,
            showAddress("http")
        );

    if (
        cfg.toObject().https &&
        cfg.toObject().https.key &&
        cfg.toObject().https.cert
    ) {
        const key = fs.readFileSync(
            path.join(process.cwd(), cfg.toObject().https.key, "utf8")
        );
        const cert = fs.readFileSync(
            path.join(process.cwd(), cfg.toObject().https.cert),
            "utf8"
        );
        const ca = cfg.toObject().https.ca
            ? fs.readFileSync(
                  path.join(process.cwd(), cfg.toObject().https.ca),
                  "utf8"
              )
            : undefined;
        httpsServer = https
            .createServer({ key, cert, ca }, app)
            .listen(cfg.toObject().https.port || 8443, showAddress("http"));
    }
    return { httpServer, httpsServer, app, ...res };
};
