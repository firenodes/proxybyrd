import fs from "fs";

export const isValid = (p: string) =>
    fs.existsSync(p) && fs.lstatSync(p).isFile();
