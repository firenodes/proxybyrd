import { NextFunction } from "express";

export const defaultHandler = (_req, _res, next: NextFunction) => next();
