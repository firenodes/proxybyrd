import express from "express";
import path from "upath";
import { isValid } from "../util";
import execa from "execa";

export const phpServeHandler = async (
    fPath: string,
    urlPath: string,
    res: express.Response,
    next: express.NextFunction
) => {
    const fIndexPath = path.join(fPath, path.dirname(urlPath), "index.php");
    const fPhpPath = path.join(
        fPath,
        path.dirname(urlPath),
        `${path.basename(urlPath)}.php`
    );
    try {
        if (isValid(fPhpPath)) return res.send(await execPhp(fPhpPath));
        else if (isValid(fIndexPath))
            return res.send(await execPhp(fIndexPath));
        else next();
    } catch (err) {
        next(err);
    }
};
const execPhp = async (fPath: string, ...args: string[]) => {
    const { stdout } = await execa("php", [fPath, ...args], { shell: true });
    return stdout.toString();
};
