import { WriteConfig } from "@theoparis/config";
import { ImprovedError } from "@toes/core";
import { Request, Response, NextFunction } from "express";
import { Config } from "../../../config";

export const authHandler = (config: WriteConfig<Config>) => (
    req: Request,
    res: Response,
    next: NextFunction
) => {
    const apiKey: string = (req.headers.authorization ||
        req.query.apiKey) as string;
    if (apiKey && apiKey === config.toObject().apiKey) return next();

    const error = new ImprovedError({ message: "Unauthorized", code: 401 });
    res.status(401);
    return next(error);
};
