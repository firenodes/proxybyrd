import { Config, Route, Service } from "../../../config";
import { logger } from "../../../logger";
import express, {
    Request,
    Response,
    Application,
    RequestHandler,
} from "express";
import { authHandler } from "./auth";
import { WriteConfig } from "@theoparis/config";
import vhost from "vhost-ts";
import { createProxyMiddleware } from "http-proxy-middleware";
import rewrite from "express-urlrewrite";
import path from "upath";
import { createServeHandler } from "../serve";
import { Options } from "../../../config/options";

const onProxyError = (err, req, res) => {
    const code = err.code;

    if (res.writeHead && !res.headersSent)
        if (/HPE_INVALID/.test(code)) res.writeHead(502);
        else
            switch (code) {
                case "ECONNRESET":
                case "ENOTFOUND":
                case "ECONNREFUSED":
                    res.writeHead(504);
                    break;
                default:
                    res.writeHead(500);
            }

    res.end(`Error occured while trying to proxy to: ${req.url}`);
};

export const handleRoute = ({
    route,
    configPath,
}: { route: Route } & Options) => {
    if (route.type && route.type)
        switch (route.type) {
            case "proxy":
                return createProxyMiddleware({
                    target: route.url,
                    onError: onProxyError,
                });
            case "send":
                return (_req: Request, res: Response) => {
                    const to = route as Record<string, unknown>;
                    return res
                        .status(to.status as number)
                        .header(
                            "content-type",
                            typeof to.data === "string"
                                ? "text/plain"
                                : "application/json"
                        )
                        .send(
                            typeof to.data === "string"
                                ? to.data
                                : JSON.stringify(to.data)
                        );
                };
            case "serve":
                return createServeHandler(
                    path.join(path.dirname(configPath), route.path)
                );

            case "render":
                return (_req: Request, res: Response) =>
                    res.sendFile(
                        path.join(
                            path.dirname(configPath),
                            (route as Record<string, unknown>).path as string
                        )
                    );
        }
};

export const getServiceRoutes = ({
    service,
    config,
    configPath,
}: {
    service: Service;
} & Options) => {
    const routes: RequestHandler[] = [];

    if (service.auth || (service.auth === undefined && config.toObject().auth))
        routes.push(authHandler(config));
    if (service.rewrite)
        routes.push(
            rewrite(service.rewrite.from || service.path, service.rewrite.to)
        );
    if (Array.isArray(service.routes))
        service.routes.forEach((r) =>
            routes.push(handleRoute({ route: r, config, configPath }))
        );
    else
        routes.push(handleRoute({ route: service.routes, configPath, config }));
    return routes;
};

export const handleService = ({
    app,
    config,
    configPath,
}: {
    app: Application;
    config: WriteConfig<Config>;
    configPath: string;
}) => ([name, service]: [string, Service]) => {
    logger.debug(`service: ${name} at ${service.path}`);

    const routeConfig = getServiceRoutes({ service, config, configPath });

    if (service.domain)
        (typeof service.domain === "string"
            ? [service.domain]
            : service.domain
        ).forEach((d) =>
            app.use(
                vhost(d, (req, res, next) => {
                    if (service.path === req.path)
                        routeConfig.forEach((r) => r(req, res, next));
                })
            )
        );
    else app.all(service.path, ...routeConfig);
};
export * from "./auth";
