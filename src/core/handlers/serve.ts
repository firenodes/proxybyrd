import path from "upath";
import fs from "fs";
import { Request, Response, NextFunction } from "express";
import { phpServeHandler } from "./php";
import { isValid } from "../util";

export const createServeHandler = (fPath: string) => {
    return async (req: Request, res: Response, next: NextFunction) => {
        const urlPath = req.originalUrl.endsWith("/")
            ? req.originalUrl.slice(0, -1)
            : req.originalUrl;
        const file = path.join(fPath, urlPath);
        if (req.originalUrl === "/" && isValid(path.join(fPath, "index.html")))
            return res.sendFile(path.join(fPath, "index.html"));
        else if (isValid(file)) return res.sendFile(file);
        else {
            const fIndexPath = path.join(
                fPath,
                path.dirname(urlPath),
                "index.html"
            );
            const fHtmlPath = path.join(
                fPath,
                path.dirname(urlPath),
                `${path.basename(urlPath)}.html`
            );
            if (isValid(fHtmlPath)) return res.sendFile(fHtmlPath);
            else if (isValid(fIndexPath)) return res.sendFile(fIndexPath);
            await phpServeHandler(fPath, urlPath, res, next);
        }
    };
};
